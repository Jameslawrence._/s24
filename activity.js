
db.users.find
(
    {
        $or: 
        [
            {firstName : {$regex: "s",$options: "$i"}},
            {lastName : {$regex: "d",$options: "$i"}}
        ]
    },
    {
        _id: 0,
        firstName: 1,
        lastName: 1
    }   
)



db.users.find
(
    {
        $and: 
        [
            {department :"Human Resources"},
            {age : {$gte : 50}}
        ]
    },
    {
    }   
)

db.users.find
(
    {
        $and: 
        [
            {firstName :{$regex: "e",$options: "$i"}} ,
            {age : {$lte : 30}}
        ]
    },
    {
    }   
)





db.users.insertMany
([
    {
       firstName : "Nikola",
       lastName : "Tesla",
       age: 64,
       department: "Communication" 
    },
    {
       firstName : "Marie",
       lastName : "Curie",
       age: 28,
       department: "Media and Arts"    
    },
    {
       firstName : "Charles",
       lastName : "Darwin",
       age: 51,
       department: "Human Resources"
    },
    {
       firstName : "Rene",
       lastName : "Descartes",
       age: 50,
       department: "Media and Arts" 
    },
    {
       firstName : "Albert",
       lastName : "Einstein",
       age: 51,
       department: "Human Resources" 
    },
    {
       firstName : "Michael",
       lastName : "Faraday",
       age: 30,
       department: "Communication" 
    }
])